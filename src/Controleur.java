import java.awt.Component;
import java.util.*;

public class Controleur {
    private static final long serialVersionUID = 1L;
    Modele modele;
    View view;

    ControleurAdapter  jc;


    /**
     * Setter du modele
     */
    public void setModele(Modele modele){
        this.modele=modele;
    }

    /**
     * Fonction qui demande au modèle de mettre à jour le modèle (grille)
     */
    public void setRequest(ArrayList<Integer> position) {
        modele.PlayMove(position.get(0), position.get(1));
    }

}

