
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JPanel;
import java.util.*;


public class ControleurAdapter extends JPanel implements ActionListener {
    private static final long serialVersionUID = 1L;
    private Controleur controleur;
    private View view;

    /**
     * Constructeur basique
     */
    public ControleurAdapter(Controleur controleur, View v) {
        this.controleur  = controleur;
        this.view = v;
    }

    /**
     * Fonction qui demande au controleur d'executer l'action qui correspond au bouton sur lequel on a appuyé
     */
    public void actionPerformed(ActionEvent e) {
        ArrayList<Integer> position = view.getPosition(e);
        controleur.setRequest(position);
        
    }

}
