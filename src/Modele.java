import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Modele  {

    /**
     * Contructeur basique
     */
    public Modele() {
        this.board = new char[3][3];
        this.movesCount = 9;
        this.playerId = 1;
    }

    private View v;
    private int playerId;
    private int movesCount;
    private char[][] board;
    private String message;


    /**
     * Fonction qui initialise la référence à la classe view
     */
    public void registerView(View v) {
        this.v = v;
    }

    /**
     * Getter du playerID
     */
    public int getPlayerId() {
        return playerId;
    }

    /**
     * Setter du playerID
     */
    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    /**
     * Getter du nombre de coups restants
     */
    public int getMovesCount() {
        return movesCount;
    }

    /**
     * Setter du nombre de coups restants
     */
    public void setMovesCount(int movesCount) {
        this.movesCount = movesCount;
    }

    /**
     * Getter du message affiché
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter du message affiché
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Fonction qui met à jour le modèle avec le nouveau coup joué par l'humain,
     * et le coup joué par l'ordinateur en réaction
     */
    public void PlayMove(int x, int y) {

        if(getMovesCount() > 0){
            setPlayerId(1);
            board[x][y] = 'X';

            // Réduit de 1 le nombre de coups restants
            setMovesCount(--movesCount);

            //Vérifie si le joueur vient de gagner ...
            if(finDePartie(x, y)) {
                setMessage("Fin de partie : l'humain gagne !");
                v.finDePartie(x, y, board[x][y], getMessage());
            }
            //... ou s'il vient de provoquer un match nul
            else if(getMovesCount()==0) {
                this.ResetModel();

            }
            //Sinon, met à jour la grille et l'ordinateur joue à son tour
            else {
                v.update(x, y, board[x][y], getMessage());
                setPlayerId(2);
                int numcase;
                numcase = getMeilleureCase();
                board[numcase/3][numcase%3] = 'O';
                // Réduit de 1 le nombre de coups restants
                setMovesCount(--movesCount);

                //Vérifie si l'ordinateur vient de gagner ...
                if(finDePartie(numcase/3, numcase%3)) {
                    setMessage("Fin de partie : l'ordinateur gagne !");
                    v.finDePartie(numcase/3, numcase%3, board[numcase/3][numcase%3], getMessage());
                }
                //... ou s'il vient de provoquer un match nul
                else if(getMovesCount()==0) {
                    this.ResetModel();

                }
                //Sinon, met à jour la grille
                v.update(numcase/3, numcase%3, board[numcase/3][numcase%3], getMessage());
            }


        }

    }



    /**
     * Fonction qui vérifie s'il y a un gagnant
     */
    public boolean finDePartie(int x, int y) {
        int countRow = 0;
        int countCol = 0;
        int countLDiag = 0;
        int countRDiag = 0;
        char symbol;
        //Selectionne un symbole en fonction du joueur qui est en train de jouer
        if(getPlayerId() %2 !=0)
            symbol = 'X';
        else
            symbol = 'O';

        //Compte les symboles alignés sur les différents axes (horizontal, vertical, diagonal)
        for(int i=0; i<board.length;i++) {
            if(board[x][i] == symbol)
                countRow++;
            if(board[i][y] == symbol)
                countCol++;
            if(board[i][i] == symbol)
                countRDiag++;
            if(board[board.length-1-i][i] == symbol)
                countLDiag++;
        }

        //Déclare la partie terminée s'il y a 3 symboles alignés sur l'un des axes
        if(countCol==board.length || countRow==board.length
                || countLDiag == board.length || countRDiag == board.length)
            return true;

        return false;
    }

    /**
     * Fonction qui réinitialise le modèle
     */
    public void ResetModel() {
        movesCount = 9;
        setPlayerId(1);
        setMessage("");
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j] = '\0';
            }
        }
        v.resetGame();
    }



    /**
     *
     * @return la meilleure case a jouer pour l'ordinateur
     */
    private int getMeilleureCase() {

        /*
         * liste des meilleures cases
         */

        List<Integer> cases = new LinkedList<Integer>();
        List<Integer> vide = new LinkedList<Integer>();


        /*
         * on etablit la liste des meilleures cases en testant chaque case
         */

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {


                if (v.getButtonText(i, j).equals("X")) {
                    cases.add(1);
                }
                else if (v.getButtonText(i, j).equals("O")) {
                    cases.add(-1);
                }
                else {
                    vide.add(i*3+j);
                    cases.add(0);

                }
            }
        }


        //Si la case du milieu n'est pas jouée on la joue
        if (cases.get(4)==0) {
            return 4;
        }

        //Coup gagnant sur les lignes
        for (int i = 0; i < 3; i++) {
            if (cases.get(i * 3)+ cases.get(1+i*3) + cases.get(2+i*3)== -2) {
                if (cases.get(i * 3)==0){return(i * 3);}
                else if (cases.get(1+i*3)==0){return(1+i*3);}
                else {return(2+i*3);}
            }
        }

        //Coup gagnant sur les colonnes
        for (int i = 0; i < 3; i++) {
            if (cases.get(i)+ cases.get(i+3) + cases.get(i+6)== -2) {
                if (cases.get(i)==0){return(i);}
                else if (cases.get(i+3)==0){return(i+3);}
                else {return(i+6);}
            }
        }

        //Coup gagnant sur la diagonale 1
        if (cases.get(2)+ cases.get(4) + cases.get(6)== -2) {
            if (cases.get(2) == 0) {
                return (2);
            } else if (cases.get(4) == 0) {
                return (4);
            } else {
                return (6);
            }
        }

        //Coup gagnant sur la diagonale 2
        if (cases.get(0)+ cases.get(4) + cases.get(8)== -2) {
            if (cases.get(0) == 0) {
                return (0);
            } else if (cases.get(4) == 0) {
                return (4);
            } else {
                return (8);
            }
        }


        //Empêcher un coup gagnant sur les lignes
        for (int i = 0; i < 3; i++) {
            if (cases.get(i * 3)+ cases.get(1+i*3) + cases.get(2+i*3)== 2) {
                if (cases.get(i * 3)==0){return(i * 3);}
                else if (cases.get(1+i*3)==0){return(1+i*3);}
                else {return(2+i*3);}
            }
        }

        //Empêcher un coup gagnant sur les colonnes
        for (int i = 0; i < 3; i++) {
            if (cases.get(i)+ cases.get(i+3) + cases.get(i+6)== 2) {
                if (cases.get(i)==0){return(i);}
                else if (cases.get(i+3)==0){return(i+3);}
                else {return(i+6);}
            }
        }

        //Empêcher un coup gagnant sur la diagonale 1
        if (cases.get(2)+ cases.get(4) + cases.get(6)== 2) {
            if (cases.get(2) == 0) {
                return (2);
            } else if (cases.get(4) == 0) {
                return (4);
            } else {
                return (6);
            }
        }

        //Empêcher un coup gagnant sur la diagonale 2
        if (cases.get(0)+ cases.get(4) + cases.get(8)== 2) {
            if (cases.get(0) == 0) {
                return (0);
            } else if (cases.get(4) == 0) {
                return (4);
            } else {
                return (8);
            }
        }


        int rand;
        rand = RANDOM.nextInt(vide.size());
        return vide.get(rand);
    }

    private static final Random RANDOM = new Random();
}
