import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.*;


public class View extends JPanel {
    private ControleurAdapter adapter;
    private JFrame gui;
    private JButton[][] blocks;
    private JTextArea playerturn;

    private static final long serialVersionUID = 1L;
    private JLabel value;

    /**
     * Constructeur qui initialise l'interface graphique (JFrame)
     */
    public View(){
        this.gui = new JFrame("Morpion");
        this.blocks = new JButton[3][3];
        this.playerturn = new JTextArea();
        // Appel de la methode initialiser (voir plus bas)
        initialiser();

        this.setPreferredSize(new Dimension(200, 30));
        this.setBorder(BorderFactory.createLineBorder(Color.black));
        value=new JLabel();
        Font police = new Font("Arial", Font.BOLD, 20);
        value.setFont(police);
        value.setHorizontalAlignment(JLabel.RIGHT);
        value.setPreferredSize(new Dimension(200, 20));
        this.add(value);
    }

    /**
     * Fonction qui lie les listeners aux boutons
     */
    public void setActionListener(Controleur c) {
        this.adapter = new ControleurAdapter(c,this);
        for(int row = 0; row<3 ;row++) {
            for(int column = 0; column<3 ;column++) {
                blocks[row][column].addActionListener(adapter);
            }
        }
    }

    /**
     * Fonction qui initialise la disposition des boutons
     */
    public void initialiser () {
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setSize(new Dimension(300, 300));
        gui.setResizable(true);

        JPanel gamePanel = new JPanel(new FlowLayout());
        JPanel game = new JPanel(new GridLayout(3,3));
        gamePanel.add(game, BorderLayout.CENTER);
        JPanel options = new JPanel(new FlowLayout());
        JPanel messages = new JPanel(new FlowLayout());
        messages.setBackground(Color.white);
        gui.add(gamePanel, BorderLayout.NORTH);
        gui.add(options, BorderLayout.CENTER);
        gui.add(messages, BorderLayout.SOUTH);
        messages.add(playerturn);
        playerturn.setText("Le joueur humain commence");

        for(int ligne = 0; ligne<3 ;ligne++) {
            for(int colonne = 0; colonne<3 ;colonne++) {
                blocks[ligne][colonne] = new JButton();
                blocks[ligne][colonne].setPreferredSize(new Dimension(75,75));
                blocks[ligne][colonne].setText("");
                game.add(blocks[ligne][colonne]);

            }
        }

        // On rend le gui visible
        gui.setVisible(true);

    }

    /**
     * Fonction qui obtient les coordonnées (x,y) du boutton sur lequel on a appuyé
     */
    public ArrayList<Integer> getPosition(ActionEvent e) {
        ArrayList<Integer> position = new ArrayList<Integer>();
        for(int row = 0; row<3 ;row++) {
            for(int column = 0; column<3 ;column++) {
                if(e.getSource() == blocks[row][column]) {
                    position.add(row);
                    position.add(column);
                }
            }
        }
        return position;
    }

    /**
     * Fonction qui met à jour la view avec le bon message et le bon symbole
     */
    public void update(int row, int column, char symbol, String message) {
        blocks[row][column].setText(Character.toString(symbol));
        blocks[row][column].setEnabled(false);
        playerturn.setText(message);

    }

    /**
     * Fonction qui bloque la view s'il y a un gagnant ou si il y a match nul
     */
    public void finDePartie(int row, int column, char symbol, String message) {
        blocks[row][column].setText(Character.toString(symbol));
        blocks[row][column].setEnabled(false);
        for(int i = 0; i<3 ;i++) {
            for(int j = 0; j<3 ;j++) {
                blocks[i][j].setEnabled(false);
            }
        }
        playerturn.setText(message);

    }

    /**
     * Fonction qui reinitialise la partie s'il y a match nul
     */
    public void resetGame() {
        for(int row = 0;row<3;row++) {
            for(int column = 0;column<3;column++) {
                blocks[row][column].setText("");
                blocks[row][column].setEnabled(true);
            }
        }
        playerturn.setText("Match nul, à toi de jouer !!");
    }

    /**
     * Getter qui récupère la valeur ("", "X" ou "O") d'une case de la grille
     */
    public String getButtonText(int i, int j) {
        return blocks[i][j].getText();
    }


}
