public class Main {
    public static void main(String[] args) {
        // Création des composants MVC
        Controleur c = new Controleur();
        View v = new View();
        Modele m = new Modele();
        // Fait le lien entre les composants MVC
        m.registerView(v);
        c.setModele(m);
        v.setActionListener(c);
    }

}
